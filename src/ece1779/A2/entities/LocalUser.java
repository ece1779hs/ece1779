package ece1779.A2.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.appengine.api.users.User;

@Entity
public class LocalUser implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	
	private String nickname;
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "creator", fetch = FetchType.EAGER)
	private List<PollTopic> ownedTopics = new ArrayList<PollTopic>();
	
	public LocalUser() {}
	
	public LocalUser(User currentUser) {
		this.id = currentUser.getUserId();
		this.nickname = currentUser.getNickname();
		this.email = currentUser.getEmail();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((nickname == null) ? 0 : nickname.hashCode());
		result = prime * result
				+ ((email == null) ? 0 : email.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LocalUser)) {
			return false;
		}
		
		LocalUser user = (LocalUser) obj;
		if (nickname.equals(user.getNickname()) && 
			email.equals(user.getEmail())) {
			return true;
		}
		return false;
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public List<PollTopic> getOwnedTopics() {
		return ownedTopics;
	}

	public void setOwnedTopics(List<PollTopic> ownedTopics) {
		this.ownedTopics = ownedTopics;
	}
}
