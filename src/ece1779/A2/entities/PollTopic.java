package ece1779.A2.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.google.appengine.api.datastore.Key;

@Entity
public class PollTopic implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key key;
	
	private String title;
	private Boolean isClosed;
	private Date addDate;
	private Long totalVotes;
	
	@Transient
	private String strKey;
	
	@Transient
	private Boolean isUserVoted;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private LocalUser creator;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "topic", fetch = FetchType.EAGER)
	private Set<PollItem> items = new HashSet<PollItem>();
	
	@Basic(fetch = FetchType.EAGER)
	private Set<String> votedUserIds = new HashSet<String>();
	
	public PollTopic() {}
	
	public PollTopic(String title) {
		this.title = title;
		this.isClosed = false;
		this.addDate = new Date();
		this.totalVotes = 0L;
	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result
//				+ ((title == null) ? 0 : title.hashCode());
//		result = prime * result
//				+ ((isClosed == null) ? 0 : isClosed.hashCode());
//		result = prime * result
//				+ ((addDate == null) ? 0 : addDate.hashCode());
//		result = prime * result
//				+ ((totalVotes == null) ? 0 : totalVotes.hashCode()) ;
//		result = prime * result
//				+ ((creator == null) ? 0 : creator.hashCode());
//		return result;
//	}
//	
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj) {
//			return true;
//		}
//		if (obj == null) {
//			return false;
//		}
//		if (!(obj instanceof PollTopic)) {
//			return false;
//		}
//		
//		PollTopic topic = (PollTopic) obj;
//
//		if (title.equals(topic.getTitle()) &&
//			creator.equals(topic.getCreator()) &&
//			isClosed.equals(topic.getIsClosed()) &&
//			addDate.equals(topic.getAddDate()) && 
//			totalVotes.equals(topic.getTotalVotes())) {
//			return true;
//		}
//		return false;
//		
//	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getIsClosed() {
		return isClosed;
	}

	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public Long getTotalVotes() {
		return totalVotes;
	}

	public void setTotalVotes(Long totalVotes) {
		this.totalVotes = totalVotes;
	}

	public String getStrKey() {
		return strKey;
	}

	public void setStrKey(String strKey) {
		this.strKey = strKey;
	}

	public Boolean getIsUserVoted() {
		return isUserVoted;
	}

	public void setIsUserVoted(Boolean isUserVoted) {
		this.isUserVoted = isUserVoted;
	}

	public LocalUser getCreator() {
		return creator;
	}

	public void setCreator(LocalUser creator) {
		this.creator = creator;
	}

	public Set<PollItem> getItems() {
		return items;
	}

	public void setItems(Set<PollItem> items) {
		this.items = items;
	}

	public Set<String> getVotedUserIds() {
		return votedUserIds;
	}

	public void setVotedUserIds(Set<String> votedUserIds) {
		this.votedUserIds = votedUserIds;
	}
}
