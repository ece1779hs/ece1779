package ece1779.A2.servlets;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;

import ece1779.A2.dao.DAOHelper;
import ece1779.A2.entities.PollTopic;

@SuppressWarnings("serial")
public class ActionServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		User currentUser = Login.checkLogin(resp);
		
		String action = req.getParameter("action");
		String topicStrKey = req.getParameter("topic");
		String itemStrKey = req.getParameter("item");
		
		DAOHelper dHelper = new DAOHelper();
		
		if (action != null) {
			if (action.equals("del")) {
				if (topicStrKey != null) {
					dHelper.removePollTopic(KeyFactory.stringToKey(topicStrKey));
					resp.sendRedirect("/pollview.jsp");
					return;
				} else if (itemStrKey != null) {
					dHelper.removePollItem(KeyFactory.stringToKey(itemStrKey));
					resp.sendRedirect("/pollvote.jsp?topic=" + topicStrKey);
					return;
				}
			} else if (action.equals("vote")) {
				if (itemStrKey != null) {
					topicStrKey = dHelper.userVoteOnItem(currentUser.getUserId(), KeyFactory.stringToKey(itemStrKey));
					resp.sendRedirect("/pollvote.jsp?topic=" + topicStrKey);
					return;
				}
			} else if (action.equals("open")) {
				if (topicStrKey != null) {
					dHelper.openTopic(KeyFactory.stringToKey(topicStrKey));
					resp.sendRedirect("/pollview.jsp");
					return;
				}
			} else if (action.equals("close")) {
				if (topicStrKey != null) {
					dHelper.closeTopic(KeyFactory.stringToKey(topicStrKey));
					resp.sendRedirect("/pollview.jsp");
					return;
				}
			}
		}
		
		resp.sendRedirect("/pollview.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		User currentUser = Login.checkLogin(resp);
		
		String topic = req.getParameter("pollTopic");
		String topicKey = req.getParameter("pollTopicKey");
		String[] options = req.getParameterValues("pollOption");
		
		DAOHelper dhelper = new DAOHelper();
		
		Key pollTopicKey = null;
		
		if (topic != null) {
			PollTopic pollTopic = new PollTopic(topic);
			dhelper.savePollTopic(pollTopic, currentUser.getUserId());
			pollTopicKey = pollTopic.getKey();
			topicKey = KeyFactory.keyToString(pollTopicKey);		
		} else if (topicKey != null) {
			pollTopicKey = KeyFactory.stringToKey(topicKey);
		}
		
		if (pollTopicKey != null) {
			dhelper.savePollItems(Arrays.asList(options), pollTopicKey);
		}
				
		resp.sendRedirect("/pollvote.jsp?topic=" + topicKey);
	}
	
}
