package ece1779.A2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import ece1779.A2.dao.DAOHelper;
import ece1779.A2.entities.LocalUser;

@SuppressWarnings("serial")
public class Login extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		User currentUser = checkLogin(resp);
		
		DAOHelper dHelper = new DAOHelper();
		LocalUser user = new LocalUser(currentUser);
		
		dHelper.saveLocalUser(user);
		
		resp.sendRedirect("/pollview.jsp");
	}

	public static User checkLogin(HttpServletResponse resp) throws IOException {
		UserService userService = UserServiceFactory.getUserService();
		User currentUser = userService.getCurrentUser();
		
		/*if (currentUser == null) {
			resp.sendRedirect("/");
		}*/
		
		return currentUser;
	}
}
