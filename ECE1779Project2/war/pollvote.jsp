<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@ page import="ece1779.project2.dao.DAOHelper" %>
<%@ page import="java.util.List" %>
<%@ page import="ece1779.project2.entities.*" %>
 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
    <link type="text/css" rel="stylesheet" href="/stylesheets/style.css"/>
    <title>ECE1779 Winter2014 Project2 Group6 - TYL Poll</title>
    <script type="text/javascript">
    $(document).ready(function() {
    	$("#moreOpsButton").click(function() {
            var newOptionInput = $("<input type='text' name='pollOption'/><br/>");
            $("#submitNewOptionsButton").before(newOptionInput);
        });
    	
    	$("form").validate({
            rules: {
                pollTopic: {
                    required: true
                },
                pollOption: {
                    required: true
                }
            }
        });
        
        $("form").submit(function() {
            $(this).find('input:text').each(function(){
                $(this).val($.trim($(this).val()));
            });
        });
    });
    </script>
</head>

<body>
    <%
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        
        String topicKey = request.getParameter("topic");
        
        if (user == null) {
            response.sendRedirect("/");
            return;
        }
        
        if (topicKey == null || topicKey.equals("null")) {
        	response.sendRedirect("/pollview.jsp");
        	return;
        }
        
        DAOHelper dHelper = new DAOHelper();
        PollTopic topic = dHelper.loadTopic(KeyFactory.stringToKey(topicKey));
        if (topic == null || topic.getItems().isEmpty()) {
        	response.sendRedirect("/pollview.jsp");
            return;
        }
        
        pageContext.setAttribute("topic", topic);
        pageContext.setAttribute("isCreator", dHelper.isCreator(user.getUserId(), KeyFactory.stringToKey(topicKey)));
        pageContext.setAttribute("isUserVoted", dHelper.isUserVotedOnTopic(user.getUserId(), KeyFactory.stringToKey(topicKey)));
    %>
    <div id="pageheader">
        <h1>TLY Poll</h1>
        <p class="userinfo">
            Welcome, <%= user.getNickname() %> &middot
            <a href="/">Home</a> &middot
            <a href="/ERDiagram.jsp">Diagram</a> &middot
            <a href="<%= userService.createLogoutURL("/") %>">Logout</a>
        </p>
        <hr/>
    </div>
    
    <div id="body"> 
        <p><c:out value="${topic.title}" /></p>
    
        <c:forEach var="item" items="${topic.items}">
            <p class="topic">
                <c:if test="${isCreator}">
                    <a href="/poll?action=del&item=<c:out value='${item.strKey}'/>">Delete</a> 
                </c:if>
                <c:if test="${!isUserVoted && !topic.isClosed}">
                    <a href="/poll?action=vote&item=<c:out value='${item.strKey}'/>">Vote</a>
                </c:if>
                <span class="title"><c:out value="${item.option}" /></span><br/> 
                <c:if test="${isUserVoted || topic.isClosed}">
                    <span class="count">Votes: <c:out value="${item.votes}" /></span>
                    <c:set var="votePercent" value="${1.0 * item.votes / topic.totalVotes}" />
                    <div class="chart" style="width: <c:out value="${item.votes * 440.0 / topic.totalVotes + 40}" />px;"><fmt:formatNumber type="percent" maxFractionDigits="1" value="${votePercent}" /></div>
                </c:if>            
            </p>
        </c:forEach>
        
        <c:if test="${isCreator}">
        <p style="padding-top: 20px">Add options: <input type="button" value="More" id="moreOpsButton" /></p>
            <form action="/poll" method="post" >
                <input type="text" name="pollTopicKey" style="display:none" value="<c:out value='${topic.strKey}'/>"/><br/>
                <input type="text" name="pollOption" /><br/>
                <input type="submit" value="Submit" id="submitNewOptionsButton"/>
            </form>     
        </c:if>
    </div>  

    <div id="pagefooter">
        ECE1779 Winter2014 Group6 Project2 - Yufei Tang, Kin-sing Lee, Yue Yu - TLY Poll      
    </div>
</body>
</html>