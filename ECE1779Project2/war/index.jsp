<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="/stylesheets/style.css"/>
    <title>ECE1779 Project2 Group6 - TYL Poll</title>
</head>

<body>
    <%
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        
        if (user != null) {
        	response.sendRedirect("/login");
        	return;
        }
    %>

    <div id="pageheader">
        <h1>EYL Poll</h1>
        <p class="userinfo">
            <a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Login</a>
        </p>
        <hr>
    </div>

    <div id="body">
        
        <p>please login first</p>
      

        <div id="pagefooter">
            ECE1779 Group6 Project2 - Yufei Tang, Kin-sing Lee, Yue Yu
        </div>
    </div>
</body>
</html>