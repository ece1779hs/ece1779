<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@ page import="ece1779.project2.dao.DAOHelper" %>
<%@ page import="java.util.List" %>
<%@ page import="ece1779.project2.entities.*" %>
 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
    <link type="text/css" rel="stylesheet" href="/stylesheets/style.css"/>
    <title>ECE1779 Winter2014 Project2 Group6 - TYL Poll</title>
    <script type="text/javascript">
    </script>
</head>

<body>
    <%
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        
        if (user == null) {
            response.sendRedirect("/");
            return;
        } 

    %>
    <div id="pageheader">
        <h1>TLY Poll</h1>
        <p class="userinfo">
            Welcome, <%= user.getNickname() %> &middot
            <a href="/">Home</a> &middot
            <a href="/ERDiagram.jsp">Diagram</a> &middot
            <a href="<%= userService.createLogoutURL("/") %>">Logout</a>
        </p>
        <hr/>
    </div>
    
    <div id="body"> 
        <p>Entity-Relation Diagram</p>
        <img alt="Entity-Relation Diagram" src="/ER_Diagram.png" width="50%" height="50%" />
    </div>  

    <div id="pagefooter">
        ECE1779 Winter2014 Group6 Project2 - Yufei Tang, Kin-sing Lee, Yue Yu - TLY Poll
        
    </div>
</body>
</html>