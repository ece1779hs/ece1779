<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@ page import="ece1779.project2.dao.DAOHelper" %>
<%@ page import="java.util.List" %>
<%@ page import="ece1779.project2.entities.*" %>
 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
    <link type="text/css" rel="stylesheet" href="/stylesheets/style.css"/>
    <title>ECE1779 Winter2014 Project2 Group6 - TYL Poll</title>
    <script type="text/javascript">
    $(document).ready(function() {
    	$("div.alltopic").hide();
    	
    	$("a.showalltopic, a.showmytopic").bind("click", function() {
    		$("div.alltopic, div.mytopic").hide();
    		
    		if ($(this).attr("class") == "showalltopic") {
    			$("div.alltopic").show();
    		} else {
    			$("div.mytopic").show();
    		}
    	});
    	
    	$("#moreOpsButton").click(function() {
    		var newOptionInput = $("<input type='text' name='pollOption'/><br/>");
    		$("#submitNewTopicButton").before(newOptionInput);

    	});
    	
    	$(".addnewtopic").validate({
    		rules: {
    			pollTopic: {
    				required: true
    			},
    			pollOption: {
    				required: true
    			}
    		}
    	});
    	
    	$("form").submit(function() {
    		$(this).find('input:text').each(function(){
    			$(this).val($.trim($(this).val()));
    		});
    	});
    });
    </script>
</head>

<body>
    <%
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        
        if (user == null) {
        	response.sendRedirect("/");
        	return;
        } 
        
        DAOHelper dHelper = new DAOHelper();
        List<PollTopic> allTopics = dHelper.loadAllTopics();
        List<PollTopic> myTopics = dHelper.loadTopicsByUser(user.getUserId());
        pageContext.setAttribute("allTopics", allTopics);
        pageContext.setAttribute("myTopics", myTopics);

    %>
    <div id="pageheader">
        <h1>TLY Poll</h1>
        <p class="userinfo">
            Welcome, <%= user.getNickname() %> &middot
            <a href="/">Home</a> &middot
            <a href="/ERDiagram.jsp">Diagram</a> &middot
            <a href="<%= userService.createLogoutURL("/") %>">Logout</a>
        </p>
        <hr/>
    </div>
    
    <div id="body"> 
        <div class="alltopic" >
	        <p>All Topics &middot <a href="#" class="showmytopic">My Topics</a></p>
	
	        <c:forEach var="topic" items="${allTopics}">
	            <p class="topic">
	               <c:choose>
	                   <c:when test="${topic.isClosed }">
	                       <a href="/pollvote.jsp?topic=<c:out value='${topic.strKey}'/>">View Results</a>
	                   </c:when>
	                   <c:otherwise>
	                       <a href="/pollvote.jsp?topic=<c:out value='${topic.strKey}'/>">Participate</a>
	                   </c:otherwise>
	               </c:choose>
	                
	                <span class="title"><c:out value="${topic.title}" /></span>
	                <c:if test="${topic.isClosed }">
	                   <span class="closed">CLOSED</span>
	                </c:if>
	                <br/>
	                <span class="count">Votes: <c:out value="${topic.totalVotes }"/></span><br/>
	                <span class="desc">Created by <c:out value="${topic.creator.nickname}" /> on 
	                <fmt:formatDate pattern="MM/dd/yyyy" value="${topic.addDate}"/></span><br/>	                
	            </p>
	        </c:forEach>
        </div>
        
        <div class="mytopic" >
            <p><a href="#" class="showalltopic">All Topics</a> &middot My Topics</p>
            
            <c:forEach var="topic" items="${myTopics}">
                <p class="topic">
                    <c:choose>
                        <c:when test="${!topic.isClosed}">
                            <a href="/poll?action=close&topic=<c:out value='${topic.strKey}'/>">Close</a>
                            <a href="/pollvote.jsp?topic=<c:out value='${topic.strKey}'/>">View Results</a>
                        </c:when>
                        <c:otherwise>
                            <a href="/poll?action=open&topic=<c:out value='${topic.strKey}'/>">Open</a>
                            <a href="/pollvote.jsp?topic=<c:out value='${topic.strKey}'/>">Participate</a>
                        </c:otherwise>
                    </c:choose>
                    <a href="/poll?action=del&topic=<c:out value='${topic.strKey}'/>">Delete</a>
                    
                    <span class="title"><c:out value="${topic.title}" /></span><br/>
                    <span class="count">Votes: <c:out value="${topic.totalVotes}"/></span><br/>
                    <span class="desc">Created at <fmt:formatDate pattern="MM/dd/yyyy" value="${topic.addDate}"/></span><br/>
                </p>
            </c:forEach>
            
            <p style="padding-top: 20px">Create new topic:</p>
            <form class="addnewtopic" action="/poll" method="post" >
                <input type="text" name="pollTopic" /><br/>
                <p>Options: <input type="button" value="More" id="moreOpsButton" /></p>
                <input type="text" name="pollOption" /><br/>
                <input type="text" name="pollOption" /><br/>
                <input type="submit" value="Submit" id="submitNewTopicButton"/>
            </form>
        </div>
        
    </div>  

    <div id="pagefooter">
        ECE1779 Winter2014 Group6 Project2 - Yufei Tang, Kin-sing Lee, Yue Yu - TLY Poll
        
    </div>
</body>
</html>