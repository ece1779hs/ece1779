package ece1779.project2.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.google.appengine.api.datastore.Key;


@Entity
public class PollItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key key;
	
	@Transient
	private String strKey;
	
	private String option;
	private Long votes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private PollTopic topic;
	
	public PollItem() {}
	
	public PollItem(String option) {
		this.option = option;
		this.votes = 0L;
	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result
//				+ ((option == null) ? 0 : option.hashCode())
//				+ ((topic == null) ? 0 : topic.hashCode());
//		return result;
//	}
//	
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj) {
//			return true;
//		}
//		if (obj == null) {
//			return false;
//		}
//		if (!(obj instanceof PollItem)) {
//			return false;
//		}
//		
//		PollItem item = (PollItem) obj;
//		
//		if (option.equals(item.getOption()) &&
//			topic.equals(item.getTopic())) {
//			return true;
//		}
//		return false;
//	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public String getStrKey() {
		return strKey;
	}

	public void setStrKey(String strKey) {
		this.strKey = strKey;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public Long getVotes() {
		return votes;
	}

	public void setVotes(Long votes) {
		this.votes = votes;
	}

	public PollTopic getTopic() {
		return topic;
	}

	public void setTopic(PollTopic topic) {
		this.topic = topic;
	}
}