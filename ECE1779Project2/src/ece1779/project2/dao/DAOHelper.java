package ece1779.project2.dao;

import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

import ece1779.project2.entities.LocalUser;
import ece1779.project2.entities.PollItem;
import ece1779.project2.entities.PollTopic;

public class DAOHelper {
	
	protected EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}
	
	protected MemcacheService getSyncMemcacheService() {
		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
	    syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
	    return syncCache;
	}
	
	public void saveLocalUser(LocalUser user) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			em.persist(user);
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}
	}
	
	public void savePollTopic(PollTopic topic, String creatorId) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			LocalUser user = em.find(LocalUser.class, creatorId);
			if (topic != null && user != null) {
				topic.setCreator(user);
				user.getOwnedTopics().add(topic);
				em.persist(topic);
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
	}
	
	public void savePollItems(List<String> items, Key topicKey) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (items != null && topic != null) {
				MemcacheService syncCache = getSyncMemcacheService();
				syncCache.delete(topicKey);
				for (String item : items) {
					PollItem pollItem = new PollItem(item);
					pollItem.setTopic(topic);
					topic.getItems().add(pollItem);
					em.persist(pollItem);
				}
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}
	}
	
	public LocalUser loadUserById(String userId) {
		LocalUser user = null;
		
		// Using the synchronous cache
		MemcacheService syncCache = getSyncMemcacheService();
	    user = (LocalUser) syncCache.get(userId); // read from cache
		
	    if (user == null) {
	    	// Get wanted user from GAE database
			EntityManager em = getEntityManager();
			EntityTransaction txn = em.getTransaction();
			
			try {
				txn.begin();
				user = em.find(LocalUser.class, userId);
				txn.commit();
			} finally {
				if (txn.isActive()) {
					txn.rollback();
				}
				em.close();
			}
			
			if (user != null) {
				// Populate cache
				syncCache.put(userId, user);
			}
	    }

		return user;
	}
	
	public void closeTopic(Key topicKey) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (topic != null && !topic.getIsClosed()) {
				MemcacheService syncCache = getSyncMemcacheService();
				syncCache.delete(topic.getKey());
				topic.setIsClosed(true);
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}		
	}
	
	public void openTopic(Key topicKey) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (topic != null && topic.getIsClosed()) {
				MemcacheService syncCache = getSyncMemcacheService();
				syncCache.delete(topic.getKey());
				topic.setIsClosed(false);
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
	}
	
	public String userVoteOnItem(String userId, Key itemKey) {
		String topicStrKey = null;
		
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			LocalUser user = em.find(LocalUser.class, userId);
			PollItem item = em.find(PollItem.class, itemKey);
			if (user != null && item != null) {
				PollTopic topic = item.getTopic();
				if (topic != null && !topic.getVotedUserIds().contains(userId)) {
					MemcacheService syncCache = getSyncMemcacheService();
					syncCache.delete(topic.getKey());
					item.setVotes(item.getVotes() + 1);
					topic.setTotalVotes(topic.getTotalVotes() + 1);
					topic.getVotedUserIds().add(userId);
					topicStrKey = KeyFactory.keyToString(topic.getKey());
				}
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
		return topicStrKey;
	}
	
	public void removePollTopic(Key topicKey) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (topic != null) {
				MemcacheService syncCache = getSyncMemcacheService();
				syncCache.delete(topic.getKey());
				LocalUser creator = topic.getCreator();
				if (creator.getOwnedTopics().contains(topic)) {
					creator.getOwnedTopics().remove(topic);
				}
				em.remove(topic);
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
	}
	
	public void removePollItem(Key itemKey) {
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollItem item = em.find(PollItem.class, itemKey);
			if (item != null) {
				PollTopic topic = item.getTopic();
				if (topic != null && topic.getItems().contains(item)) {
					MemcacheService syncCache = getSyncMemcacheService();
					syncCache.delete(topic.getKey());
					topic.setTotalVotes(topic.getTotalVotes() - item.getVotes());
					topic.getItems().remove(item);
				}
				em.remove(item);
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}
		
	}
	
	public List<PollTopic> loadAllTopics() {
		List<PollTopic> topics = null;
		
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			TypedQuery<PollTopic> q = em.createQuery("SELECT p FROM " + PollTopic.class.getName()
						+ " p", PollTopic.class);
			topics = q.getResultList();
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}
		for (PollTopic topic : topics) {
			topic.setStrKey(KeyFactory.keyToString(topic.getKey()));
			for (PollItem item : topic.getItems()) {
				item.setStrKey(KeyFactory.keyToString(item.getKey()));
			}
		}
		
		return topics;
	}
	
	public PollTopic loadTopic(Key key) {
		PollTopic topic = null;
		
		// Using the synchronous cache
		MemcacheService syncCache = getSyncMemcacheService();
		topic = (PollTopic) syncCache.get(key); // read from cache
		
		if (topic == null) {
			EntityManager em = getEntityManager();
			EntityTransaction txn = em.getTransaction();
			try {
				txn.begin();
				topic = em.find(PollTopic.class, key);
				txn.commit();
			} finally {
				if (txn.isActive()) {
					txn.rollback();
				}
				em.close();
			}	
			
			if (topic != null) {
				topic.setStrKey(KeyFactory.keyToString(topic.getKey()));
				for (PollItem item : topic.getItems()) {
					item.setStrKey(KeyFactory.keyToString(item.getKey()));
				}
				
				// Populate cache 
				syncCache.put(key, topic);
			}
		}
		return topic;
	}
	
	public List<PollTopic> loadTopicsByUser(String userId) {
		List<PollTopic> topics = null;
		
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			LocalUser creator = em.find(LocalUser.class, userId);
			if (creator != null) {
				TypedQuery<PollTopic> q = em.createQuery("SELECT p FROM " + PollTopic.class.getName()
						+ " p WHERE p.creator = ?1", PollTopic.class);
				q.setParameter(1, creator);
				topics = q.getResultList();
			}		
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
		
		for (PollTopic topic : topics) {
			topic.setStrKey(KeyFactory.keyToString(topic.getKey()));
			for (PollItem item : topic.getItems()) {
				item.setStrKey(KeyFactory.keyToString(item.getKey()));
			}
		}
		
		return topics;
	}
	
	public List<PollItem> loadItemsByTopic(Key topicKey) {
		List<PollItem> items = null;
		
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (topic != null) {
				TypedQuery<PollItem> q = em.createQuery("SELECT i FROM " + PollItem.class.getName()
						+ " i WHERE i.topic = ?1", PollItem.class);
				q.setParameter(1, topic);
				items = q.getResultList();
			}		
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
		
		for (PollItem item : items) {
			item.setStrKey(KeyFactory.keyToString(item.getKey()));
		}
		
		return items;
	}
	
	public boolean isUserVotedOnTopic(String userId, Key topicKey) {
		boolean flag = false;
		
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (topic != null && topic.getVotedUserIds().contains(userId)) {
				flag = true;
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}	
		
		return flag;
	}
	
	public boolean isCreator(String userId, Key topicKey) {
		boolean flag = false;
		
		EntityManager em = getEntityManager();
		EntityTransaction txn = em.getTransaction();
		try {
			txn.begin();
			PollTopic topic = em.find(PollTopic.class, topicKey);
			if (topic != null && topic.getCreator().getId().equals(userId)) {
				flag = true;
			}
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
			em.close();
		}
		
		return flag;
	}
}
